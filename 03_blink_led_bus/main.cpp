#include "mbed.h"

BusOut ledbus(PA_4, PB_10, PA_13);
/*
int ledvalues[] = {
	0b00000000, //all led off
	0b00000001, //Green Led on
	0b00000010, //yellow led on
	0b00000100, //red led on
};

int main() {
	int count = sizeof(ledvalues) / sizeof(ledvalues[0]);
    while(1) {
        for(int i=0 ; i <count; i++) {
					//ledbus.write(ledvalues[i]);
					ledbus = ledvalues[i];
					wait(0.5);
				}
    }
}
*/

int ledvalues[] = {
	0b00000000,
	0b00000001,
	0b00000010,
	0b00000011,
	0b00000100,
	0b00000101,
	0b00000110,
	0b00000111,
};

int main() {
	int count = sizeof(ledvalues) / sizeof(ledvalues[0]);
	
	while(1) {
		for(int i = 0 ; i < count; i++) {
			ledbus = ledvalues[i];
			wait(0.3);
		}
	}
}
