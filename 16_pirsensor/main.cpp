#include "mbed.h"

Serial pc(USBTX,USBRX); //tera term
DigitalIn PIRSensor(D5); //PIR Motion Sensor
DigitalOut led(PA_13); // green led

float motion;

int main() {
	pc.baud(115200); //board late
	printf("start...\n"); //get and printf like stdout  fprintf(stdout,"rrkkr");
	
	while(1) {
		motion = PIRSensor; // motion = PIRSensor.read();
		
		if(motion != 0 ) {
			led = 1; //led on
			
			printf("motion detected..\n");
		}
		else {
			led = 0; // led off
			printf("motion not detected..\n");
		}
		
		wait(0.3);
	}
}
