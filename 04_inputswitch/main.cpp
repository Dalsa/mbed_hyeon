#include "mbed.h"

DigitalOut led1(PA_13); //Green LED
DigitalIn btn1(PA_14); //First Button

int main() {
    
	while(1) {
		if(btn1) {
			led1 = 1;
		}
		else {
			led1 = 0;
		}
	}
}
