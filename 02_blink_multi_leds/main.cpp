#include "mbed.h"

DigitalOut led1(PA_13); //green
DigitalOut led2(PB_10); //yellow
DigitalOut led3(PA_4); //red

int main() {
		
    while(1) {
			led1 = 1;
			led2 = 1;
			wait(0.1);
			
			led1 = 0;
			led3 = 1;
			wait(0.1);
			
			led2 = 0;
			led1 = 1;
			wait(0.1);
			
			led3 = 0;
    }
}
