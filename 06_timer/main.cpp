#include "mbed.h"

DigitalOut led1(PA_13); //Green LED
DigitalIn btn1(PA_14); //First Button
Timer tmr; //timer
int dt; //dealy time

typedef enum {
	NO_EDGE = 0, RISING_EDGE, FALLING_EDGE
}edge_t;

edge_t detectBtn1edge() {
	static int prevState = 1;
	
	edge_t edge = NO_EDGE;
	
	if(btn1 != prevState) {
		wait(0.05);
		if(btn1 != prevState) {
			if(btn1 == 1) edge = RISING_EDGE;
			else edge = FALLING_EDGE;
			prevState = btn1;
		}
	}
	return edge;
}

edge_t detectBtn2edge() {
	static int prevState = 1;
	
	edge_t edge = NO_EDGE;
	
	if(btn1 != prevState) {
		wait(0.05);
		if(btn1 != prevState) {
			if(btn1 == 1) edge = RISING_EDGE;
			else edge = FALLING_EDGE;
			prevState = btn1;
		}
	}
	return edge;
}
//main starter
int main() {
	tmr.start();
	//anjung
	while(1) {
		edge_t edge = detectBtn2edge();
		if(edge == FALLING_EDGE) tmr.reset();
		else if(edge == RISING_EDGE) dt = tmr.read_ms();
		
		if(dt != 0) {
			tmr.reset();
			while(tmr.read_ms() < dt) {
				led1=1;
				wait(0.3);
				led1=0;
				wait(0.3);
			}
		}
	}
}

