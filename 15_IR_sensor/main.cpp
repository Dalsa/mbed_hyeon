#include "mbed.h"

Serial pc(USBTX,USBRX); //tera term
AnalogIn IRSensor(PC_5); //IRSensor is Analog

#define BRING_SET 0.45
//my device setting another

double map(double x, double in_min, double in_max, double out_min, double out_max) { //mapping middle value
	double scale = (out_max - out_min) / (in_max - in_min);
	return scale * x;
}

int main() {
	pc.baud(115200); //board late
	double ir, volt, dist;
	
	//avg 5 is normal 
	while(1) {
		ir = 0; // reset
		for (int i = 0; i < 0; i++) {
			ir += IRSensor;
		}
		ir = ir / 5;
		
		ir = IRSensor;
		pc.printf("IR Seonsor value : %4.2f \n", ir);
		
		if (ir != 0) {
			volt = map(ir,0.0, 1.0, 0, 3200);
			dist = ((27.61f / (volt - 0.1696f)) * 1000);
		}
		else {
			dist = 0;
		}
		
		pc.printf("distance %5.2f cm\r\n\n", dist);
		
		wait(0.5);
	}
}
