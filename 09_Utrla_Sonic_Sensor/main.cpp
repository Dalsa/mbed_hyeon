#include "mbed.h"

#define UNIT_NAME_CONSTANT 58.0 //sang su
#define REMIT_ECHO_TIME 30000 //30 second
Timer t;
int dt = 0;

DigitalOut trigger(PB_6);
DigitalIn echo(PA_8);
Serial pc(USBTX,USBRX); //terminal output input

int main() {
	pc.baud(115200); //baud rate clock time? telecom delay
	pc.printf("Start start start \n"); //test
	t.start(); //timer start
	while(1) {
		
		//trigger utlra sonic sensor
		trigger = 1; //trigger start
		wait_ms(40); //micro second dealy
		trigger = 0; //trigger stop
		
		//measure echo signal
		while(!echo) {} //higt echo wait
		t.reset();
		while(echo) {} //low echo wait
		dt = t.read_us();
			
		//Filtering
		if (dt < REMIT_ECHO_TIME){
			//print measured distance
			pc.printf("Measured echo time = %d\n", dt);
			pc.printf("Distance = %6.2f\n\n", dt / UNIT_NAME_CONSTANT);
			wait(0.5);
		}
		else {
			pc.printf("No object detected.. \n");
		}
	}
}
