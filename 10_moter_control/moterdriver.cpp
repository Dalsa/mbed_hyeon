#include "moterdriver.h"


Motor::Motor(PinName pwm, PinName dir) 
			: _pwm(pwm), _dir(dir)	
{
	//this->_pwm = pwm;
	//this->_dir = dir; 
	
	
	_pwm.period(0.001);	//Pulse Freq. 1KHz
	_pwm = 0;
	
	_dir = 0;
	status = STOP;
	
}

void Motor::forward(double speed) {
	if (status == BACKWORD) {
		_pwm = 0;
		wait(0.2);
	}
	_dir = 1;
	_pwm = abs(speed);
	status = FORWORD;
}

void Motor::backward(double speed) {
	if (status == FORWORD) {
		_pwm = 0;
		wait(0.2);
	}
	_dir = 1;
	_pwm = abs(speed);
	status = BACKWORD;
}

void Motor::stop() {
	_pwm = 0;
	status = STOP;
}
