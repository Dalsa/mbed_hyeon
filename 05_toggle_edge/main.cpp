#include "mbed.h"

DigitalOut led1(PA_13); //Green LED
DigitalIn btn1(PA_14); //First Button
DigitalIn btn2(PB_7); //second Button
BusOut leds(PA_13, PB_10, PA_4);
//toggle switch

typedef enum {
	NO_EDGE = 0, RISING_EDGE, FALLING_EDGE
}edge_t;

edge_t detectBtn1edge() {
	static int prevState = 1;
	
	edge_t edge = NO_EDGE;
	
	if(btn1 != prevState) {
		wait(0.05);
		if(btn1 != prevState) {
			if(btn1 == 1) edge = RISING_EDGE;
			else edge = FALLING_EDGE;
			prevState = btn1;
		}
	}
	return edge;
}

edge_t detectBtn2edge() {
	static int prevState = 1;
	
	edge_t edge = NO_EDGE;
	
	if(btn2 != prevState) {
		wait(0.05);
		if(btn2 != prevState) {
			if(btn2 == 1) edge = RISING_EDGE;
			else edge = FALLING_EDGE;
			prevState = btn2;
		}
	}
	return edge;
}

int main() {
    
	while(1) {
		if (detectBtn1edge() == FALLING_EDGE){
			leds = 0b00000111;
		}
		if (detectBtn2edge() == FALLING_EDGE) {
			leds = 0;
		}
	}
}
