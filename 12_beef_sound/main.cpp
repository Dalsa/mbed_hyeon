#include "mbed.h"

DigitalOut butter(PC_9);

#define c 1915 //261 hz

Timer t;

int notePlayDelay[] = {
	1915,1700,1519,1432,1275,1136,1014,956
};

void playsound(char note, double playtime) {
	int timeHigh = (note == 'C') ? notePlayDelay[7] : notePlayDelay[note-'a'];
	t.reset();
    while(1) {
			t.reset();
			while(t.read() < playtime) {
				butter = 1;
				wait_us(timeHigh);
				butter = 0;
				wait_us(timeHigh);
			}
			wait(0.1);
    }
}

int main() {
	char song[] = "abc";
	int size = sizeof(song) / sizeof(song[0]);
	
	t.start();
	
	while(1) {
		for (int i = 0 ; i < size; i++) {
			playsound(song[i], 1.0);
			wait(0.5);
		}
	}
	wait(10);
}
