#include "mbed.h"

//DigitalOut led(PA_5); //digital signal out
DigitalOut led1(PA_13);
DigitalOut led2(PB_10);
DigitalOut led3(PA_4);


int main() {
	//setup -- initialation
	
	//main loop 
	while(1) {
		led1 = 1; //LED on
		led2 = 1;
		led3 = 0;
		wait(0.5); // 1 = 1 second
		led1 = 0; //LED off
		led2 = 1;
		led3 = 1;
		wait(0.5);
		led1.write(1); //LED on
		wait(0.5); // 1 = 1 second
		led1.write(0); //LED off
		wait(0.5);
		
		led2 = 1; //LED on
		wait(0.5); // 1 = 1 second
		led2 = 0; //LED off
		wait(0.5);
		led2.write(1); //LED on
		wait(0.5); // 1 = 1 second
		led2.write(0); //LED off
		wait(0.5);
		
		led3 = 1; //LED on
		wait(0.5); // 1 = 1 second
		led3 = 0; //LED off
		wait(0.5);
		led3.write(1); //LED on
		wait(0.5); // 1 = 1 second
		led3.write(0); //LED off
		wait(0.5);
	}
}
