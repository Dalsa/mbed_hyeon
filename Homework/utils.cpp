#include "mbed.h"
#include "motor.h"
#include "utils.h"

//Ultraspnic_range_finder
DigitalOut sonic_Trig(D10);
DigitalIn sonic_Echo(D7);
float i;
Timer tt;

//rgb_led
PwmOut r(A1);
PwmOut g(PC_6);
PwmOut b(A3);

//rgb_led_control
void rgb_Red(){
	r.period(0.001);
	g.period(0.001);
	b.period(0.001);
	
	r = 1;
	g = 0;
	b = 0;
}

void rgb_Green() {
	r.period(0.001);
	g.period(0.001);
	b.period(0.001);
	
	r = 0;
	g = 1;
	b = 0;
}


//motor
Motor::Motor(PinName pwm, PinName dir):
    _pwm(pwm), _dir(dir)
{
    //set initial condition of PWM
    _pwm.period(0.001);
    _pwm = 0;
    
    //Initial condition of output enables
    _dir = 0;
    sign = 0;
}

void Motor::forward(double speed) {
    float temp = 0;
    
    if(sign == -1) { //unroll
        _pwm = 0;
        wait(0.2);
    }
    
    _dir = 1;
    temp = abs(speed);
    _pwm = temp;
    sign = 1;
}

void Motor::backward(double speed) {
    float temp = 0;
    
    if(sign == 1) { //roll
        _pwm = 0;
        wait(0.2);
    }
    
    _dir = 0;
    temp = abs(speed);
    _pwm = temp;
    sign = -1;
}

void Motor::stop(void) {
    _pwm = 0;
    sign = 0;    
}

//Ultraspnic_range_finder
int Ultra(){
    tt.start();
    while(1) {
				wait(2);
        sonic_Trig = 1;
        wait(0.00002);
        sonic_Trig = 0;
        
        while(!sonic_Echo);
        
        tt.reset();
        while(sonic_Echo);
        i = tt.read_us();
			
				i = i / 58;
        
        return i;
    }
}

//ir sesonr map
double map(double x, double in_min, double in_max, double out_min, double out_max)
{
  double scale = (x - in_min) / (in_max - in_min);
  return scale * (out_max - out_min) + out_min;
}

