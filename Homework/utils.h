#ifndef UTILS_H
#define UTILS_H

// State Values
typedef enum {
    PROTECT_OPEN = 1,
    PROTECT_CLOSE = 0
} status_protect;

//IRSensor
double map(double x, double in_min, double in_max, double out_min, double out_max);

//Ultra Sonic
int Ultra();

//rgb_led
void rgb_Red();
void rgb_Green();

#endif
