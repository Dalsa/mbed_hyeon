#include "mbed.h"

Serial pc(USBTX,USBRX);
AnalogIn sound(A5); //microphone
DigitalOut GreenLed(PA_13);

#define SOUND_THERSBLD 0.76 //my device setting another

int main() {
	pc.baud(115200);
	
	while(1) {
		if (sound.read() > SOUND_THERSBLD) {
			pc.printf("Laoubess : %f \n", sound.read());
			GreenLed = 1;
			wait(1.0);
		}
		else {
			GreenLed = 0;
		}
		wait(0.3);
	}
}
