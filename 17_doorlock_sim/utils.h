#ifndef utils
#define utils
#include "mbed.h"


//reset device
void init_device();
//buzzer
void playNote(char note, double play_time);
//button edge 
uint8_t readButtonEdge(); 

uint8_t getButtonInput(); 

void displayLeds(int ledValue);

void start_blink_led1();

void stop_blink_led1();

void start_blink_led3();

void stop_blink_led3();

#endif
