#include "mbed.h"
#include "utils.h"
#include "doorlock_sim.h"

Serial pc(USBTX,USBRX);

state_t state;
pi_state_t pi_state;

int passwd[4] = {1,2,1,2}; 

int processPsswdInput() {
	static int pw_in[4];
	static int pi_state = FIRST_INPUT;
	int status = CONTINUE;
	uint8_t edge; //??
	int i;
	
	if(btc.read() > BUTTON_INPUT_TIMEOUT) {
		pi_state = PW_FAILURE;
	}
	
	switch(pi_state) {
		case FIRST_INPUT:
			edge = getButtonInput();
			if(edge & 0x10) { //check if falling dge is occured
				edge &= 0x07; //low 3bit masking
				pw_in[0] = (edge == 4) ? 3 : edge;
				
				pi_state = SECOND_INPUT;
				btc.reset();
			}
			break;
		case SECOND_INPUT:
			edge = getButtonInput();
			if(edge & 0x10) {
				edge &= 0x07;
				pw_in[1] = (edge == 4) ? 3 : edge;
				
				pi_state = THIRD_INPUT;
				btc.reset();
			}
			break;
		case THIRD_INPUT:
			edge = getButtonInput();
			if(edge & 0x10) {
				edge &= 0x07;
				pw_in[2] = (edge == 4) ? 3 : edge;
				
				pi_state = FOURTH_INPUT;
				btc.reset();
			}
			break;
		case FOURTH_INPUT:
			edge = getButtonInput();
			if(edge & 0x10) {
				edge &= 0x07;
				pw_in[3] = (edge == 4) ? 3 : edge;
				
				pi_state = PW_SUCCESS;
				btc.reset();
			}
			break;
		case PW_SUCCESS:
			status = DOOR_OPEN;
			pi_state = FIRST_INPUT;
			break;
		case PW_FAILURE:
			status = DOOR_CLOSED;
			pi_state = FIRST_INPUT;
			break;
	}
}

int processCloseButtonInput() {
	int status = CONTINUE;
  
  if (btc.read() > BUTTON_INPUT_TIMEOUT) {
    status = FAILED;
  }
  else {
		int edge = getButtonInput();
    if (edge == btcN1_RISING) {
		  if (btc.read() > MODE_CHANGE_TIMEOUT) {
			  status = SUCCEED;
				BUTTON_BEEP();
		  }
		  else {
			  status = FAILED;
		  }
		}
		else if (edge == btcN1_FALLING) {
			btc.reset();
		}	
	}

  return status;
}	

void setup() {
	pc.baud(115200);
	init_device();
	
	state = DOOR_CLOSED;
	pi_state = FIRST_INPUT;
	DISPLAY_DOOR_CLOSED();
}

int main() {
	setup();
	
  while(1) {
		switch(state) {
			case DOOR_CLOSED:
				DISPLAY_DOOR_CLOSED();
				if(getButtonInput() == btcN3_FALLING) {
					start_blink_led3();
					btc.reset();
					state = PASSWD_INPUT;
				}
				break;
			case PASSWD_INPUT:
				switch(processPsswdInput()) {
					case FAILED:
						FAILURE_BEEP();
						stop_blink_led1();
						state = DOOR_CLOSED;
						break;
					case SUCCEED:
						SUCCESS_BEEP();
						stop_blink_led3();
						state = DOOR_OPEN;
						break;
				}
				break;
			case DOOR_OPEN:
				switch(processCloseButtonInput()) {
					case FAILED:
						FAILURE_BEEP();
						stop_blink_led3();
						state = DOOR_OPEN;
						break;
					case SUCCEED:
						SUCCESS_BEEP();
						stop_blink_led1();
						state = DOOR_CLOSED;
						break;
					default:
						break;
				}
				break;
			case CLOSE_BUTTON_INPUT:
				switch(processCloseButtonInput()) {
					case FAILED:
						FAILURE_BEEP();
						stop_blink_led3();
						state = DOOR_OPEN;
						break;
					case SUCCEED:
						SUCCESS_BEEP();
						stop_blink_led1();
						state = DOOR_CLOSED;
						break;
					default:
						break;
				}
				break;
			}
    }
}
