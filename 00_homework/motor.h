#ifndef MOTOR_H
#define MOTOR_H

#include "mbed.h"

//motor
class Motor {
	public:
		Motor(PinName pwm, PinName dir);
		void forward(double speed);
		void backward(double speed);
		void stop(void);
        
	protected:
		PwmOut _pwm;       // PWM pin
		DigitalOut _dir;   // motor diection pin  
		int sign;          // current motor direction
};
#endif