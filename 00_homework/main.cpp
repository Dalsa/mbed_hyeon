#include "mbed.h"
#include "utils.h"
#include "motor.h"

//motor
Motor PROTECT_MOTOR(D11, PC_8); //pwm, dir

//irsensor
AnalogIn IRSensor(PC_5);
int dist = 0;
double volt;

//status
int sp;

//main
int main() {
	sp = PROTECT_CLOSE;

	
    while(1) {
			switch(sp) {
				case PROTECT_CLOSE:
					PROTECT_MOTOR.stop();
					rgb_Red();
				
					//irsensor
					volt = map(IRSensor, 0, 1.0, 0, 3200);
					dist = (int)((27.61f / (volt - 0.1696f)) * 1000.0);
					
					if(dist <= 30) { //distance near
						sp = PROTECT_OPEN;
					}
					break;
				
				case PROTECT_OPEN:
					PROTECT_MOTOR.forward(2);
					rgb_Green();
					
					if(Ultra() < 50) {
						sp = PROTECT_CLOSE;
					}
					break;
			}
    }
}
