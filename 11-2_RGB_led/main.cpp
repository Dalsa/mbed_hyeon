#include "mbed.h"

PwmOut green(PC_6); //Green is RGB LED
PwmOut red(A1);
PwmOut blue(A3);

int main() {
	green.period(0.001);
	green = 0;
	red.period(0.001);
	red = 0;
	blue.period(0.001);
	blue = 0;
    while(1) {
       for(double i = 0.0; i < 1.0 ; i += 0.001) {
				 double p = 3 * i;
				 green = 1.0 - ((p<1.0) ? 1.0 - p : (p > 2.0) ? p - 2.0 : 0.0);
				 red = 1.0 - ((p<1.0) ? p : (p < 2.0) ? 1.0 : 2.0 - p);
				 blue = 1.0 - ((p < 1.0) ? 0.0 : (p > 2.0) ? p - 2.0 : 2.0 - p);
				 
				 wait(0.01);
			 }
    }
}
